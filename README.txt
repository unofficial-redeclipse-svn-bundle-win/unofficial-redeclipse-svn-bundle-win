The unofficial Red Eclipse SVN bundle for Windows
version 0.6

This is the unofficial Red Eclipse SVN bundle for Windows, it is designed to be
able to download and incrementally update Red Eclipse from its SVN repository,
and to run it without conflicting with another version.

* To download/update Red Eclipse, run 'update_redeclipse.bat'
  This will download Red Eclipse into the directory 'redeclipse-svn'
  or, if it already exists, update it to the latest revision

* To start Red Eclipse run 'redeclipse.bat'
  Likewise for 'redeclipse_server.bat'
  These are modified scripts which uses the directory 'home' to store the user
  configuration in
  This allows using the SVN version of Red Eclipse in tandem with a standard
  release version, without effecting eachother

If you make modifications to the downloaded copy in 'redeclipse-svn', the
update may fail, in this case, it would be appropriate to start using something
like TortoiseSVN which can handle these complications in a
point-and-click-friendly way.

TortoiseSVN should be able to use the existing redeclipse-svn folder as its
checkout if you simply point it to this folder and reset the checkout url
correctly.


This bundle was assembled by Martin Erik Werner ("arand")
<martinerikwerner@gmail.com>
Content and modifications which are unique to this bundle are available under a
Zlib license: http://www.gzip.org/zlib/zlib_license.html

This bundle uses a minimal version of Subversion® downloaded from
http://www.visualsvn.com/downloads/

Subversion and Red Eclipse both have their individual licenses, see the
documentation in their respective directory for more information.
