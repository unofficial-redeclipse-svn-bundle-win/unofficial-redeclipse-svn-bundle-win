@echo off

set co_url=svn://svn.icculus.org/redeclipse

for /f "tokens=2" %%u in ('svn\bin\svn.exe info redeclipse-svn 2^>nul ^| find "URL: "' ) do (
    if errorlevel 0 (
        if /i not "%%u" == "%co_url%" (
            echo Relocating checkout URL
            echo svn\bin\svn.exe relocate %co_url% redeclipse-svn
            svn\bin\svn.exe relocate %co_url% redeclipse-svn
        )
    )
)

echo Downloading Red Eclipse from SVN
echo Please be patient, this will take some time..
echo svn\bin\svn.exe checkout %co_url% redeclipse-svn
svn\bin\svn.exe checkout %co_url% redeclipse-svn
pause
